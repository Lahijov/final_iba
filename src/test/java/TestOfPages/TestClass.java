package TestOfPages;

import Pages.Date_Picker;
import Pages.Menu_Items;
import Pages.Progress_Bar;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

// NOTE CHROMEDRIVER VERSION THAT USED IS: 91.0.4472.19

public class TestClass {
    private WebDriver driver;
    private Actions action;
    private Menu_Items menu_items;
    private Date_Picker date_picker;
    private Progress_Bar progress_bar;

    @BeforeMethod
    void setUp() {
        driver = new ChromeDriver();
        action = new Actions(driver);
        driver.get("https://www.globalsqa.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    private WebDriver getDriver() {
        return driver;
    }

    @AfterMethod
    void teardown() {
        getDriver().quit();

    }

    @Test
    void VerifyingNumberOfItems() {

        //Arrange
        int expected_cells_for_each_column = 6;
        menu_items = new Menu_Items(getDriver());
        WebDriverWait wait_untill = new WebDriverWait(driver, 10);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        //Act
        wait_untill.until(ExpectedConditions.visibilityOf(menu_items.Tester_Hub));
        action.moveToElement(menu_items.Tester_Hub).perform();

        wait_untill.until(ExpectedConditions.visibilityOf(menu_items.Demo_Testing_Site));
        action.moveToElement(menu_items.Demo_Testing_Site).click().perform();

        js.executeScript("window.scrollBy(0,350)");

        wait_untill.until(ExpectedConditions.visibilityOfAllElements(menu_items.First_step));
        wait_untill.until(ExpectedConditions.visibilityOfAllElements(menu_items.Second_step));
        wait_untill.until(ExpectedConditions.visibilityOfAllElements(menu_items.Third_step));

        //Assert

        Assertions.assertTrue(menu_items.First_step.size() == expected_cells_for_each_column,
                String.format("Actual column size is '%s' but expected size is '%d'", menu_items.First_step.size(), expected_cells_for_each_column));

        Assertions.assertTrue(menu_items.Second_step.size() == expected_cells_for_each_column,
                String.format("Actual column size is '%s' but expected size is '%d'", menu_items.Second_step.size(), expected_cells_for_each_column));

        Assertions.assertTrue(menu_items.Third_step.size() == expected_cells_for_each_column,
                String.format("Actual column size is '%s' but expected size is '%d'", menu_items.Third_step.size(), expected_cells_for_each_column));

        System.out.println(String.format("Number of items in '%s' column  is '%d'", menu_items.First_Step_Text.getText(), menu_items.First_step.size()));
        System.out.println(String.format("Number of items in '%s' column  is '%d'", menu_items.Second_Step_Text.getText(), menu_items.Second_step.size()));
        System.out.println(String.format("Number of items in '%s' column  is '%d'", menu_items.Third_Step_Text.getText(), menu_items.Third_step.size()));

    }

    @Test
    void DatePicker() {

        //Arrange

        date_picker = new Date_Picker(getDriver());
        WebDriverWait wait_untill = new WebDriverWait(driver, 10);

        //Act
        wait_untill.until(ExpectedConditions.visibilityOf(date_picker.Tester_Hub));

        action.moveToElement(date_picker.Tester_Hub).perform();

        wait_untill.until(ExpectedConditions.visibilityOf(date_picker.Demo_Testing_Site));

        action.moveToElement(date_picker.Demo_Testing_Site).perform();

        action.moveToElement(date_picker.DatePicker).click().perform();

        wait_untill.until(ExpectedConditions.visibilityOf(date_picker.iframe));
        driver.switchTo().frame(date_picker.iframe);

        date_picker.DateInputField.click();

        wait_untill.until(ExpectedConditions.visibilityOf(date_picker.nextMonth));
        date_picker.nextMonth.click();

        wait_untill.until(ExpectedConditions.visibilityOf(date_picker.dayOfCalendar));
        date_picker.dayOfCalendar.click();

        String dateActual = date_picker.DateInputField.getAttribute("value");

        Date date = new Date(121, 6, 17);

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String dateExpected = formatter.format(date);

        //Assert

        Assertions.assertEquals(dateActual, dateExpected,
                String.format("Actual date size is '%s' but expected date is '%s'", dateActual, dateExpected));

        System.out.println("Selected and Actual Date is  : " + dateActual);
        System.out.println("Expected Date is: " + dateExpected);

    }

    @Test
    void ProgressBar()  {

        //Arrange

        progress_bar = new Progress_Bar(getDriver());
        WebDriverWait wait_untill = new WebDriverWait(driver, 10);

        //Act
        wait_untill.until(ExpectedConditions.visibilityOf(progress_bar.Tester_Hub));
        action.moveToElement(progress_bar.Tester_Hub).perform();

        wait_untill.until(ExpectedConditions.visibilityOf(progress_bar.Demo_Testing_Site));
        action.moveToElement(progress_bar.Demo_Testing_Site).perform();

        wait_untill.until(ExpectedConditions.visibilityOf(progress_bar.ProgressBar));
        action.moveToElement(progress_bar.ProgressBar).click().perform();

        driver.switchTo().frame(progress_bar.iframe);

        wait_untill.until(ExpectedConditions.visibilityOf(progress_bar.StartDowloandButoon));
        action.moveToElement(progress_bar.StartDowloandButoon).click().perform();

        wait_untill.until(ExpectedConditions.visibilityOf(progress_bar.completed));

        //Assert

        Assertions.assertTrue(progress_bar.completed.isDisplayed(), "There is a problem during downloading process");
        if (progress_bar.completed.isDisplayed() == true) {
            System.out.println("The condition is true");
        }


    }
}