package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Progress_Bar extends BasePage{

    public Progress_Bar(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//a[@href=\"https://www.globalsqa.com/demo-site/progress-bar/\" and span[text()=\"Progress Bar\"]]")
    public WebElement ProgressBar;

    @FindBy(xpath = "//iframe[@class=\"demo-frame lazyloaded\"]")
    public WebElement iframe;

    @FindBy(xpath = "//button[@id=\"downloadButton\" and @class=\"ui-button ui-corner-all ui-widget\"]")
    public WebElement StartDowloandButoon;

    @FindBy(xpath = "//div[@class=\"progress-label\"]")
    public WebElement completed;






}
