package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    public BasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@href=\"https://www.globalsqa.com/demo-site/\" and @class=\"no_border sub_menu_parent\"]")
    public WebElement Demo_Testing_Site;

    @FindBy(xpath = "(//a[@href=\"https://www.globalsqa.com/testers-hub/\"])[1]")
    public WebElement Tester_Hub;

}
