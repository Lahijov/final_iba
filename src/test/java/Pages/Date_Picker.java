package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Date_Picker extends BasePage{



    @FindBy(xpath = "//a[@href=\"https://www.globalsqa.com/demo-site/datepicker/\" and span[text()=\"DatePicker\"]]")
    public WebElement DatePicker;

    @FindBy(xpath = "//iframe[@class=\"demo-frame lazyloaded\"]")
    public WebElement iframe;

    @FindBy(xpath = "//*[@id=\"datepicker\" and @type=\"text\" and @class=\"hasDatepicker\"]")
    public WebElement DateInputField;

    @FindBy(xpath = "//*[@class=\"ui-datepicker-next ui-corner-all\"]")
    public WebElement nextMonth;

    @FindBy(xpath = "(//a[@class=\"ui-state-default\"])[17]")
    public WebElement dayOfCalendar;



    public Date_Picker(WebDriver driver) {
        super(driver);
    }
}
