package Pages;

 import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Menu_Items extends BasePage {


    public Menu_Items(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "(//div[@class=\"price_column \"])[1]//ul//li[@class=\"price_footer\"]")
    public List<WebElement> First_step;

    @FindBy(xpath = "(//div[@class=\"price_column \"])[2]//ul//li[@class=\"price_footer\"]")
    public List<WebElement> Second_step;

    @FindBy(xpath = "(//div[@class=\"price_column \"])[3]//ul//li[@class=\"price_footer\"]")
    public List<WebElement> Third_step;

    @FindBy(xpath = "(//li[@class=\"price_column_title\"])[1]")
    public WebElement First_Step_Text;

    @FindBy(xpath = "(//li[@class=\"price_column_title\"])[2]")
    public WebElement Third_Step_Text;

    @FindBy(xpath = "(//li[@class=\"price_column_title\"])[3]")
    public WebElement Second_Step_Text;


}